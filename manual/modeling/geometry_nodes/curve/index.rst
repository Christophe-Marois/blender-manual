
###############
  Curve Nodes
###############

Nodes that only operate on curves.

.. toctree::
   :maxdepth: 2

   Read <read/index.rst>
   Sample <sample/index.rst>
   Write <write/index.rst>

-----

.. toctree::
   :maxdepth: 2

   Operations <operations/index.rst>
   Primitives <primitives/index.rst>
   Topology <topology/index.rst>
